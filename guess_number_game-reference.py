import random

guess = int(input("Choose a number between 1 and 50 \n"))
#print(guess)
if guess > 50 or guess < 1:
    print("That's not a number between 1 and 50")
else:
    newguessanswer = input("Is " + str(guess) + " your final answer? (correct, higher, lower) \n")

    if newguessanswer == "correct":
        print("Cool, sticking with " + str(guess))
    elif newguessanswer == "higher":
        guess += 1
        print("Your new number is " + str(guess))
    elif newguessanswer == "lower":
        guess -= 1
        print("Your new number is " + str(guess))
    else:
        print("That wasn't one of the options.")

answer = random.randrange(50)
if guess == answer:
    print("Wow! You guessed it!")
else:
    print("The answer was ", answer)
