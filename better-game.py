from random import randint

name = input("Hi! What is your name? ")

for i in range (1,6,1):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Guess " + str(i) + ":", name, "were you born in", month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif i < 5:
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")
